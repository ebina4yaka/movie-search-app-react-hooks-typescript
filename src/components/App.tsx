import React, { useState, useEffect } from 'react'
import './App.css'
import Header from './Header'
import Movie, { MovieDetail } from './Movie'
import Search from './Search'

type ApiResponse = {
  Search: MovieDetail[]
  Response: string
  Error: null
}

const MOVIE_API_URL = 'https://www.omdbapi.com/?s=man&apikey=4a3b711b'

export default (): React.ReactElement => {
  const [loading, setLoading] = useState(true)
  const [movies, setMovies] = useState(Array<MovieDetail>())
  const [errorMessage, setErrorMessage] = useState(null)

  useEffect(() => {
    fetch(MOVIE_API_URL)
      .then((response) => response.json())
      .then((jsonResponse: ApiResponse) => {
        setMovies(jsonResponse.Search)
        setLoading(false)
      })
  }, [])

  const search = (searchValue: string): void => {
    setLoading(true)
    setErrorMessage(null)

    fetch(`https://www.omdbapi.com/?s=${searchValue}&apikey=4a3b711b`)
      .then((response) => response.json())
      .then((jsonResponse: ApiResponse) => {
        if (jsonResponse.Response === 'True') {
          setMovies(jsonResponse.Search)
          setLoading(false)
        } else {
          setErrorMessage(jsonResponse.Error)
          setLoading(false)
        }
      })
  }

  const getSearchResults = (): JSX.Element[] => {
    const list: JSX.Element[] = []
    if (loading && !errorMessage) {
      list.push(<span>loading...</span>)
    } else if (errorMessage) {
      list.push(<div className="errorMessage">{errorMessage}</div>)
    } else {
      movies.map((movie: MovieDetail) =>
        list.push(
          <Movie Title={movie.Title} Poster={movie.Poster} Year={movie.Year} />
        )
      )
    }

    return list
  }

  return (
    <div className="App">
      <Header text="HOOKED" />
      <Search search={search} />
      <p className="App-intro">Sharing a few of our favorite movies</p>
      <div className="movies">{getSearchResults()}</div>
    </div>
  )
}
