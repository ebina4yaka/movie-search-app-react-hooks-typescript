import React from 'react'

export type MovieDetail = {
  Poster: string
  Title: string
  Year: number
}

const DEFAULT_PLACEHOLDER_IMAGE =
  'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg'

export default (movie: MovieDetail): React.ReactElement => {
  const { Title: title, Year: year } = movie
  const poster =
    movie.Poster === 'N/A' ? DEFAULT_PLACEHOLDER_IMAGE : movie.Poster
  return (
    <div className="movie">
      <h2>{title}</h2>
      <div>
        <img width="200" alt={`The movie titled: ${title}`} src={poster} />
      </div>
      <p>{year}</p>
    </div>
  )
}
