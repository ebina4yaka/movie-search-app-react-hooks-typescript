import React from 'react'

type Props = {
  text: string
}

export default (props: Props): React.ReactElement => {
  const { text } = props
  return (
    <header className="App-header">
      <h2>{text}</h2>
    </header>
  )
}
